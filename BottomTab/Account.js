import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'

import Icon from 'react-native-vector-icons/Feather';

import { Container, Header,Button ,Content,Left,Right,Body,Title ,List, ListItem, Text, Separator } from 'native-base';
import {Router,Stack,Actions,Scene} from 'react-native-router-flux'
import App from './Product'

const Index = () => {
 {
    return (
      <Container>
        <Header>
            <Body>
                <Title>Тохиргоо</Title>
            </Body>
      </Header>
        <Content>
          <Separator bordered>
            <Text>Бидний тухай</Text>
          </Separator>
          <ListItem>
            <Text>Говь ХК</Text>
          </ListItem>
          <ListItem>
            <Text>Брэнд</Text>
          </ListItem>
          <ListItem>
            <Text>Салбар дэлгүүр</Text>
          </ListItem>
          <ListItem>
            <Text></Text>
          </ListItem>
          <Separator bordered>
            <Text>Туслалцаа</Text>
          </Separator>
          <ListItem>
            <Text>Холбоо барих</Text>
          </ListItem>
          <ListItem>
            <Text>Буцаан солих журам</Text>
          </ListItem>
          <ListItem>
            <Text>Ноолуур арчилгаа</Text>
          </ListItem>
        </Content>
      </Container>
    )
    }
    };

export default Index;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',
        alignItems: 'center',
        justifyContent: 'center',
    }
})
