import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'

export default class Index extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Нүүр хуудас</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ddd',
        alignItems: 'center',
        justifyContent: 'center',
    }
})
