import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export class Test extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Hehe</Text>
      </View>
    )
  }
}

export default Test

const styles = StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  item: {
    padding: 3,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    
  }
});
